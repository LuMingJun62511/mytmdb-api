import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import Genre from "../../../../api/genres/genreModel";
import User from "../../../../api/users/userModel";
import api from "../../../../index";
import genres from "../../../../seedData/genres";

const expect = chai.expect;
let db;
let login_info = {
    'username': 'user1',
    'password': 'test1'
}
let user1token;

describe("Genres endpoint", () => {
    before(() => {
        mongoose.connect(process.env.MONGO_DB, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        db = mongoose.connection;
    });

    after(async () => {
        try {
            await db.dropDatabase();
        } catch (error) {
            console.log(error);
        }
    });

    beforeEach(async () => {
        try {
            await User.deleteMany();
            // Register a user
            await request(api).post("/api/users?action=register").send(login_info);
            await Genre.deleteMany();
            await Genre.collection.insertMany(genres);
        } catch (err) {
            console.error(`failed to Load genres Data: ${err}`);
        }
    });
    afterEach(() => {
        api.close(); // Release PORT 8080
    });

    //2
    describe("GET /api/genres/tmdb/genres ", () => {
        it("should login first and all the genres plus a status 200 ", () => {
            return request(api)
                .post("/api/users?action=authenticate")
                .send(login_info)
                .expect(200)
                .then((res) => {
                    user1token = res.body.token;
                });
        });
        after(() => {
            return request(api)
                .get("/api/genres/tmdb/genres")
                .set("Authorization", user1token)
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .then((res) => {
                    expect(res.body.genres).to.be.a("array");
                    expect(res.body.genres.length).to.equal(19);
                    expect(res.body.genres[1].id).to.equal(12);
                    expect(res.body.genres[1].name).to.equal("Adventure");
                });
        });
    });
});
