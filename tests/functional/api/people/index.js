import chai from "chai";
import request from "supertest";
import api from "../../../../index";

const expect = chai.expect;
const actorid = 224513;

describe("Actors endpoint", () => {
    afterEach(() => {
        api.close(); // Release PORT 8080
    });
    describe("GET /api/people/tmdb/actorDetail/:id ", () => {
        describe("valid id ", () => {
            it("should return an actor's detail information and a status 200", (done) => {
                request(api)
                    .get(`/api/people/tmdb/actorDetail/${actorid}`)
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect(200)
                    .end((err, res) => {
                        expect(res.body.gender).to.equal(1);
                        expect(res.body.known_for_department).to.equal('Acting');
                        expect(res.body.name).to.equal('Ana de Armas');
                        done();
                    });
            });
        });
        describe("invalid id ", () => {
            it("should return a status 404", (done) => {
                request(api)
                    .get(`/api/people/tmdb/actorDetail/0`)
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect(404)
                    .expect({ success: false, msg: 'invalid id' });
                done();
            });
        });
    });

    describe("GET /api/people/tmdb/externalID/:id ", () => {
        describe("valid id ", () => {
            it("should return an actor's externalID and a status 200", (done) => {
                request(api)
                    .get(`/api/people/tmdb/externalID/${actorid}`)
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect(200)
                    .end((err, res) => {
                        expect(res.body.id).to.equal(actorid);
                        expect(res.body.wikidata_id).to.equal('Q698173');
                        expect(res.body.instagram_id).to.equal('ana_d_armas');
                        done();
                    });
            });
        });
        describe("invalid id ", () => {
            it("should return a status 404", (done) => {
                request(api)
                    .get(`/api/people/tmdb/externalID/0`)
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect(404)
                    .expect({ success: false, msg: 'invalid id' });
                done();
            });
        });
    });


    describe("GET /api/people/tmdb/actorCredits/:id ", () => {
        describe("valid id ", () => {
            it("should return an list of movies actor take part in and a status 200", (done) => {
                request(api)
                    .get(`/api/people/tmdb/actorCredits/${actorid}`)
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect(200)
                    .end((err, res) => {
                        expect(res.body.cast.length).to.equal(32);
                        expect(res.body.cast[0].id).to.equal(362579);
                        expect(res.body.cast[0].original_title).to.equal('Anabel');
                        expect(res.body.cast[0].character).to.equal('Cris');
                        done();
                    });
            });
        });
        describe("invalid id ", () => {
            it("should return a status 404", (done) => {
                request(api)
                    .get(`/api/people/tmdb/actorCredits/0`)
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect(404)
                    .expect({ success: false, msg: 'invalid id' });
                done();
            });
        });

    });

    describe("GET /api/people/tmdb/popular/:page ", () => {
        describe("valid page ", () => {
            it("should return an list of actors who are popular now and a status 200", (done) => {
                request(api)
                    .get(`/api/people/tmdb/popular/1`)
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect(200)
                    .end((err, res) => {
                        expect(res.body.page).to.equal(1);
                        expect(res.body.results).to.be.a("array");
                        expect(res.body.results.length).to.equal(20);
                        //its content changes frequently
                        done();
                    });
            });
        });

        describe("invalid page ", () => {
            it("should return a status 404", (done) => {
                request(api)
                    .get(`/api/people/tmdb/popular/0`)
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect(404)
                    .expect({ success: false, msg: 'invalid page' });
                done();
            });
        });
    })
});