import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import User from "../../../../api/users/userModel";
import api from "../../../../index";

const expect = chai.expect;
let db;
let user1token;

describe("Users endpoint", () => {
  before(() => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = mongoose.connection;
  });

  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });
  beforeEach(async () => {
    try {
      await User.deleteMany();
      // Register two users
      await request(api).post("/api/users?action=register").send({
        username: "user1",
        password: "test1",
        favourites: [238, 156]
      });
      await request(api).post("/api/users?action=register").send({
        username: "user2",
        password: "test2",
      });
    } catch (err) {
      console.error(`failed to Load user test Data: ${err}`);
    }
  });
  afterEach(() => {
    api.close();
  });
  describe("GET /api/users ", () => {
    it("should return the 2 users and a status 200", (done) => {
      request(api)
        .get("/api/users")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          expect(res.body).to.be.a("array");
          expect(res.body.length).to.equal(2);
          let result = res.body.map((user) => user.username);
          expect(result).to.have.members(["user1", "user2"]);
          done();
        });
    });
  });

  describe("POST /api/users ", () => {
    describe("For a register action", () => {
      describe("when the payload is correct", () => {
        it("should return a 201 status and the confirmation message", () => {
          return request(api)
            .post("/api/users?action=register")
            .send({
              username: "user3",
              password: "test3",
            })
            .expect(201)
            .expect({ msg: "Successful created new user.", code: 201 });
        });
        after(() => {
          return request(api)
            .get("/api/users")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((res) => {
              expect(res.body.length).to.equal(3);
              const result = res.body.map((user) => user.username);
              expect(result).to.have.members(["user1", "user2", "user3"]);
            });
        });
      });
    });
    describe("For an authenticate action", () => {
      describe("when the payload is correct", () => {
        it("should return a 200 status and a generated token", () => {
          return request(api)
            .post("/api/users?action=authenticate")
            .send({
              username: "user1",
              password: "test1",
            })
            .expect(200)
            .then((res) => {
              expect(res.body.success).to.be.true;
              expect(res.body.token).to.not.be.undefined;
              user1token = res.body.token.substring(7);
            });
        });
      });
    });
  });



  describe("GET /api/users/:userName/favourites ", () => {
    it("should return a list of ID of users's favorite movies and a status 200", (done) => {
      request(api)
        .get("/api/users/user1/favourites")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          expect(res.body).to.be.a("array");
          expect(res.body.length).to.equal(2);
          expect(res.body[0]).to.equal(238);
          expect(res.body[1]).to.equal(156);
          done();
        });
    });
  });

  describe("POST /api/users/:userName/favourites ", () => {
    describe("when it is a valid post ", () => {
      it("should return the 2 users and a status 201", (done) => {
        request(api)
          .post("/api/users/user1/favourites")
          .set("Accept", "application/json")
          .send({
            id: 555
          })
          .expect("Content-Type", /json/)
          .expect(201)
          .end((err, res) => {
            expect(res.body.favourites).to.be.a("array");
            expect(res.body.favourites.length).to.equal(3);
            expect(res.body.favourites[2]).to.equal(555);
            done();
          });
      });
    });
    describe("when it is a Duplicate post ", () => {
      it("should return a msg and a status 401", (done) => {
        request(api)
          .post("/api/users/user1/favourites")
          .set("Accept", "application/json")
          .send({
            id: 238
          })
          .expect("Content-Type", /json/)
          .expect(401)
          .expect({ code: 401, msg: 'Duplicate favo' });
        done();
      });
    });
  });


  describe("POST /api/users/:userName/favourites/delete ", () => {
    describe("when it is a valid delete ", () => {
      it("should return the a user info after deleting and a status 201", (done) => {
        request(api)
          .post("/api/users/user1/favourites/delete")
          .set("Accept", "application/json")
          .send({
            id: 238
          })
          .expect("Content-Type", /json/)
          .expect(201)
          .end((err, res) => {
            expect(res.body.favourites).to.be.a("array");
            expect(res.body.favourites.length).to.equal(1);
            done();
          });
      });
    });
    describe("when it is not a valid delete ", () => {
      it("should return a msg and a status 401", (done) => {
        request(api)
          .post("/api/users/user1/favourites/delete")
          .set("Accept", "application/json")
          .send({
            id: 555
          })
          .expect("Content-Type", /json/)
          .expect(401)
          .expect({ code: 401, msg: 'do not have' });
        done();
      });
    });
  });
});
