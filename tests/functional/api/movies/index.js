import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import api from "../../../../index";
import User from "../../../../api/users/userModel";

const expect = chai.expect;
let db;
let login_info = {
  'username': 'user1',
  'password': 'test1'
}
let user1token;
let testedMovie = 238


describe("Movies endpoint", () => {
  before(() => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = mongoose.connection;
  });

  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });

  beforeEach(async () => {
    try {
      await User.deleteMany();
      await request(api).post("/api/users?action=register").send(login_info);
    } catch (err) {
      console.error(err);
    }
  });
  afterEach(() => {
    api.close(); // Release PORT 8080
  });


  //1
  describe("GET /api/movies/tmdb/upcoming ", () => {
    it("should login first and a list of upcoming movies", () => {
      return request(api)
        .post("/api/users?action=authenticate")
        .send(login_info)
        .expect(200)
        .then((res) => {
          user1token = res.body.token;
        });
    });
    after(() => {
      return request(api)
        .get("/api/movies/tmdb/upcoming")
        .set("Authorization", user1token)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .then((res) => {
          expect(res.body.results).to.be.a("array");
          expect(res.body.results.length).to.equal(20);
        });
    });
  });

  //3
  describe("GET /api/movies/tmdb/movieImgs/:id ", () => {
    describe("when the id is valid", () => {
      it("should login first and return an movie's imgs plus a status 200", () => {
        return request(api)
          .post("/api/users?action=authenticate")
          .send(login_info)
          .expect(200)
          .then((res) => {
            user1token = res.body.token;
          });
      });
      after(() => {
        return request(api)
          .get(`/api/movies/tmdb/movieImgs/${testedMovie}`)
          .set("Authorization", user1token)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .then((res) => {
            expect(res.body.posters).to.be.a("array");
            expect(res.body.posters.length).to.equal(128);
          });
      });
    });
    describe("invalid id ", () => {
      it("should return a status 404", (done) => {
        request(api)
          .get(`/api/movies/tmdb/movieImgs/0`)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(404)
          .expect({ success: false, msg: 'invalid id' });
        done();
      });
    });
  });


  //7
  describe("GET /api/movies/tmdb/movie/:id ", () => {
    describe("when the id is valid", () => {
      it("should login first and return an movie's info plus a status 200 ", () => {
        return request(api)
          .post("/api/users?action=authenticate")
          .send(login_info)
          .expect(200)
          .then((res) => {
            user1token = res.body.token;
          });
      });
      after(() => {
        return request(api)
          .get(`/api/movies/tmdb/movie/${testedMovie}`)
          .set("Authorization", user1token)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .then((res) => {
            expect(res.body.homepage).to.equal('http://www.thegodfather.com/');
            expect(res.body.release_date).to.equal('1972-03-14');
            expect(res.body.title).to.equal('The Godfather');
          });
      });
    });
    describe("invalid id ", () => {
      it("should return a status 404", (done) => {
        request(api)
          .get(`/api/movies/tmdb/movie/0`)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(404)
          .expect({ success: false, msg: 'invalid id' });
        done();
      });
    });
  });

  //8
  describe("GET /api/movies/tmdb/movies ", () => {
    it("should login first and a list of movies", () => {
      return request(api)
        .post("/api/users?action=authenticate")
        .send(login_info)
        .expect(200)
        .then((res) => {
          user1token = res.body.token;
        });
    });
    after(() => {
      return request(api)
        .get("/api/movies/tmdb/movies")
        .set("Authorization", user1token)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .then((res) => {
          expect(res.body.results).to.be.a("array");
          expect(res.body.results.length).to.equal(20);
        });
    });
  });

  //9
  describe("GET /api/movies/tmdb/movieCredits/:id ", () => {
    describe("when the id is valid", () => {
      it("should login first and return a movie's casts plus a status 200", () => {
        return request(api)
          .post("/api/users?action=authenticate")
          .send(login_info)
          .expect(200)
          .then((res) => {
            user1token = res.body.token;
          });
      });
      after(() => {
        return request(api)
          .get(`/api/movies/tmdb/movieCredits/${testedMovie}`)
          .set("Authorization", user1token)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .then((res) => {
            expect(res.body.cast).to.be.a("array");
            expect(res.body.cast.length).to.equal(58);
            expect(res.body.cast[0].id).to.equal(3084);
            expect(res.body.cast[0].name).to.equal('Marlon Brando');
            expect(res.body.cast[0].character).to.equal('Don Vito Corleone');
          });
      });
    });
    describe("invalid id ", () => {
      it("should return a status 404", (done) => {
        request(api)
          .get(`/api/movies/tmdb/movieCredits/0`)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(404)
          .expect({ success: false, msg: 'invalid id' });
        done();
      });
    });
  });

  //11
  describe("GET /api/movies/tmdb/movieReviews/:id ", () => {
    describe("when the id is valid", () => {
      it("should login first and return a movie's reviews plus a status 200", () => {
        return request(api)
          .post("/api/users?action=authenticate")
          .send(login_info)
          .expect(200)
          .then((res) => {
            user1token = res.body.token;
          });
      });
      after(() => {
        return request(api)
          .get(`/api/movies/tmdb/movieReviews/${testedMovie}`)
          .set("Authorization", user1token)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .then((res) => {
            expect(res.body).to.be.a("array");
            expect(res.body.length).to.equal(2);
            expect(res.body[1].content.substring(0, 19)).to.equal("The best movie ever");
          });
      });
    });
  });
  describe("invalid id ", () => {
    it("should return a status 404", (done) => {
      request(api)
        .get(`/api/movies/tmdb/movieCredits/0`)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(404)
        .expect({ success: false, msg: 'invalid id' });
      done();
    });
  });
});
