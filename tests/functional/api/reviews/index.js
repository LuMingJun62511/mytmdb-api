import uniqid from 'uniqid'
import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import api from "../../../../index";

const expect = chai.expect;
let db;

let testedMovie = 238

let test_review1 = {
    id: uniqid(),
    author: "Yikun Fan",
    rating: 5,
    content: "Love this movie"
}

let test_review2 = {
    id: uniqid(),
    author: "Yikun Fan",
    rating: 2,
    content: "a piece of shit"
}

describe("Reviews endpoint", () => {
    before(() => {
        mongoose.connect(process.env.MONGO_DB, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        db = mongoose.connection;
    });

    after(async () => {
        try {
            await db.dropDatabase();
        } catch (error) {
            console.log(error);
        }
    });

    afterEach(() => {
        api.close(); // Release PORT 8080
    });
    describe("POST /api/reviews/movieReviews/:id ", () => {
        describe("post valid content", () => {
            it("should successfully post", async () => {
                await request(api)
                    .post(`/api/reviews/movieReviews/${testedMovie}`)
                    .send(test_review1)
                    .expect(201)
                    .then((res) => {
                        expect(res.body.content).to.equal(test_review1.content);
                    });
            });
        });
        describe("post invalid content", () => {
            it("should not successfully post", async () => {
                await request(api)
                    .post(`/api/reviews/movieReviews/${testedMovie}`)
                    .send(test_review2)
                    .expect(401)
                    .expect({ success: false, msg: 'Bad Content' });
            });
        });
    });
});