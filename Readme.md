# Assignment 2 - Agile Software Practice.
Yikun Fan  
20099869
## Api endpoints.
+ GET api/genres/tmdb/genres - get all genres from TMDB api

+ GET api/movies/tmdb/upcoming - get a page of upcoming movies from TMDB api
+ GET api/movies/tmdb/movieImgs/:id - get a list of imgs of a movie from TMDB api
+ GET api/movies/tmdb/movie/:id - get a movie‘s basic info from TMDB api
+ GET api/movies/tmdb/movies - get a page of movies from TMDB api
+ GET api/movies/tmdb/movieCredits/:id - get a movie's casts from TMDB api
+ GET api/movies/tmdb/movieReviews/:id - get a movie's reviews from TMDB api

+ GET api/people/tmdb/actorDetail/:id - get a actor's basic details from TMDB api
+ GET api/people/tmdb/externalID/:id - get a actor's external ID from TMDB api
+ GET api/people/tmdb/actorCredits/:id - get a list of movies actor takes part in from TMDB api
+ GET api/people/tmdb/popular/:page - get a page of popular actors from TMDB api

+ POST api/reviews/movieReviews/:id - add a reviews of a movie written by a user

+ POST api/users - register or authenticate a user
+ GET api/users/:userName/favourites - get a user's all favourite movies
+ POST api/users/:userName/favourites - add a movie to a user's favourite list
+ POST api/users/:userName/favourites/delete - remove a movie from a user's favourite list


## Test cases.
  Users endpoint
    GET /api/users 
database connected to test on ac-shynrft-shard-00-02.ivrvzka.mongodb.net
      √ should return the 2 users and a status 200 (75ms)
    POST /api/users 
      For a register action
        when the payload is correct
          √ should return a 201 status and the confirmation message (606ms)
      For an authenticate action
        when the payload is correct
          √ should return a 200 status and a generated token (187ms)
    GET /api/users/:userName/favourites 
      √ should return a list of ID of users's favorite movies and a status 200
    POST /api/users/:userName/favourites 
      when it is a valid post
        √ should return the 2 users and a status 201 (146ms)
      when it is a Duplicate post 
        √ should return a msg and a status 401
    POST /api/users/:userName/favourites/delete 
      when it is a valid delete
        √ should return the a user info after deleting and a status 201 (213ms)
      when it is not a valid delete 
        √ should return a msg and a status 401

  Movies endpoint
    GET /api/movies/tmdb/upcoming
      √ should login first and a list of upcoming movies (274ms)
    GET /api/movies/tmdb/movieImgs/:id 
      when the id is valid
        √ should login first and return an movie's imgs plus a status 200 (195ms)
      invalid id 
        √ should return a status 404
    GET /api/movies/tmdb/movie/:id 
      when the id is valid
        √ should login first and return an movie's info plus a status 200  (202ms)
      invalid id 
        √ should return a status 404
    GET /api/movies/tmdb/movies 
      √ should login first and a list of movies (252ms)
    GET /api/movies/tmdb/movieCredits/:id 
      when the id is valid
        √ should login first and return a movie's casts plus a status 200 (176ms)
      invalid id 
        √ should return a status 404
    GET /api/movies/tmdb/movieReviews/:id 
      when the id is valid
        √ should login first and return a movie's reviews plus a status 200 (174ms)
    invalid id 
      √ should return a status 404

  Actors endpoint
    GET /api/people/tmdb/actorDetail/:id
      valid id
        √ should return an actor's detail information and a status 200 (187ms)
      invalid id 
        √ should return a status 404
    GET /api/people/tmdb/externalID/:id
      valid id
        √ should return an actor's externalID and a status 200 (164ms)
      invalid id 
        √ should return a status 404
    GET /api/people/tmdb/actorCredits/:id
      valid id
        √ should return an list of movies actor take part in and a status 200 (175ms)
      invalid id 
        √ should return a status 404
    GET /api/people/tmdb/popular/:page
      valid page
        √ should return an list of actors who are popular now and a status 200 (1487ms)
      invalid page 
        √ should return a status 404

  Genres endpoint
    GET /api/genres/tmdb/genres
      √ should login first and all the genres plus a status 200  (205ms)

  Reviews endpoint
    POST /api/reviews/movieReviews/:id
      post valid content
        √ should successfully post
      post invalid content
        √ should not successfully post


  29 passing (20s)



## Independent learning.
Option B For the code coverage report, please view https://coveralls.io/gitlab/LuMingJun62511/mytmdb-api
During doing this assignment, I learned and generated a code coverage report, according the report, I found that some lines of my api never used, so I made some test for them, the code coverage report helps me to see where the tests need to be improved.


## Some important url.
+ For the gitlab, view https://gitlab.com/LuMingJun62511/mytmdb-api
+ For the staging deploy, view https://mytmdb-api-staging.herokuapp.com/
+ For the production deploy, view https://mytmdb-api-product.herokuapp.com/ 