import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const ReviewSchema = new Schema({

  id: { type: String, required: true, unique: true },
  movieId: { type: Number },
  author:  { type: String },
  rating: { type: Number },
  content: { type: String },
});

ReviewSchema.statics.findByReviewId = function (id) {
  return this.findOne({ id: id });
};

export default mongoose.model('Reviews', ReviewSchema);



