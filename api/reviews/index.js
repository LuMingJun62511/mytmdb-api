import express from 'express';
import reviewsModel from '../reviews/reviewsModel';
import asyncHandler from 'express-async-handler';

//13 
const router = express.Router();
const regex = new RegExp(/(shit|fuck)/);

router.post('/movieReviews/:id', asyncHandler(async (req, res) => {
    const movieId = parseInt(req.params.id);
    if (regex.test(req.body.content)) {
        res.status(401).json({ success: false, msg: 'Bad Content' });
        return next();
    } else {
        reviewsModel.create({
            id: req.body.id,
            movieId: movieId,
            author: req.body.author,
            rating: req.body.rating,
            content: req.body.content
        });
        res.status(201).json(req.body);
    }
}));

export default router;