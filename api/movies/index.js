import express from 'express';
import asyncHandler from 'express-async-handler';
import { getUpcomingMovies, getMovieImages, getMovie, getMovies, getMovieCredits, getMovieReviews } from '../tmdb-api';


const router = express.Router();
const regex = new RegExp(/([1-9]|[1-9][0-9]|[1-9][0-9][0-9]|[1-9][0-9][0-9][0-9]|[1-9][0-9][0-9][0-9][0-9]|[1-9][0-9][0-9][0-9][0-9][0-9])/);

//1
router.get('/tmdb/upcoming', asyncHandler(async (req, res) => {
    const upcomingMovies = await getUpcomingMovies();
    res.status(200).json(upcomingMovies);
}));

//3
router.get('/tmdb/movieImgs/:id', asyncHandler(async (req, res) => {
    const id = parseInt(req.params.id);
    if (regex.test(id)) {
        const movieImgs = await getMovieImages(id);
        res.status(200).json(movieImgs);
    } else {
        res.status(404).json({ success: false, msg: 'invalid id' });
    }
}));

//7
router.get('/tmdb/movie/:id', asyncHandler(async (req, res) => {
    const id = parseInt(req.params.id);
    if (regex.test(id)) {
        const movie = await getMovie(id);
        res.status(200).json(movie);
    } else {
        res.status(404).json({ success: false, msg: 'invalid id' });
    }
}));

//8
router.get('/tmdb/movies', asyncHandler(async (req, res) => {
    const movies = await getMovies();
    res.status(200).json(movies);
}));

//9
router.get('/tmdb/movieCredits/:id', asyncHandler(async (req, res) => {
    const id = parseInt(req.params.id);
    if (regex.test(id)) {
        const movieCredits = await getMovieCredits(id);
        res.status(200).json(movieCredits);
    } else {
        res.status(404).json({ success: false, msg: 'invalid id' });
    }
}));


//11
router.get('/tmdb/movieReviews/:id', asyncHandler(async (req, res) => {
    const id = parseInt(req.params.id);
    if (regex.test(id)) {
        const movieReviews = await getMovieReviews(id);
        res.status(200).json(movieReviews);
    } else {
        res.status(404).json({ success: false, msg: 'invalid id' });
    }
}));

export default router;