import express from "express";
import User from "./userModel";
import asyncHandler from "express-async-handler";
import jwt from 'jsonwebtoken';

const router = express.Router(); // eslint-disable-line

// Get all users
router.get("/", async (req, res) => {
  const users = await User.find();
  res.status(200).json(users);
});

// Register OR authenticate a user
router.post('/', asyncHandler(async (req, res, next) => {
  if (!req.body.username || !req.body.password) {
    res.status(401).json({ success: false, msg: 'Please pass username and password.' });
    return next();
  }
  if (req.query.action === 'register') {
    await User.create(req.body);
    res.status(201).json({ code: 201, msg: 'Successful created new user.' });
  } else {
    const user = await User.findByUserName(req.body.username);
    if (!user) return res.status(401).json({ code: 401, msg: 'Authentication failed. User not found.' });
    user.comparePassword(req.body.password, (err, isMatch) => {
      if (isMatch && !err) {
        const token = jwt.sign(user.username, process.env.SECRET);
        res.status(200).json({ success: true, token: 'BEARER ' + token });
      } else {
        res.status(401).json({ code: 401, msg: 'Authentication failed. Wrong password.' });
      }
    });
  }
}));

//15
router.get('/:userName/favourites', asyncHandler(async (req, res) => {
  const userName = req.params.userName;
  const user = await User.findByUserName(userName).populate('favourites');
  res.status(200).json(user.favourites);
}));

//15
router.post('/:userName/favourites', asyncHandler(async (req, res) => {
  const newFavourite = req.body.id;
  const userName = req.params.userName;
  const user = await User.findByUserName(userName);
  if (!user.favourites) {
    await user.favourites.push(newFavourite);
    await user.save();
    res.status(201).json(user);
  } else if (!user.favourites.includes(newFavourite)) {
    await user.favourites.push(newFavourite);
    await user.save();
    res.status(201).json(user);
  } else {
    res.status(401).json({ code: 401, msg: 'Duplicate favo' });
  }
}));

//15
router.post('/:userName/favourites/delete', asyncHandler(async (req, res) => {
  const toBeDeleted = req.body.id;
  const userName = req.params.userName;
  const user = await User.findByUserName(userName);
  if (!user.favourites.includes(toBeDeleted)) {
    res.status(401).json({ code: 401, msg: 'do not have' });
  } else {
    await user.favourites.pop(toBeDeleted);
    await user.save();
    res.status(201).json(user);
  }
}));

export default router;
